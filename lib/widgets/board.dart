import 'package:flutter/material.dart';

import 'board_item.dart';
import 'custom_modal.dart';

class Board extends StatefulWidget {
  @override
  _BoardState createState() => _BoardState();
}

class _BoardState extends State<Board> {
  List<String> boardNames = [];
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(0),
      elevation: 4,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 48,
          left: 16,
          bottom: 16,
          right: 16,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                for (String name in boardNames) BoardItem(boardName: name),
                BoardItem(onPressed: showCustomModal),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showCustomModal() {
    showDialog(
      context: context,
      builder: (ctx) => CustomModal(
        controller: controller,
        onCreate: () {
          if (controller.text.isNotEmpty)
            setState(() {
              boardNames.add(controller.text);
            });
          controller.clear();
          Navigator.pop(ctx);
        },
      ),
    );
  }
}
