import 'package:flutter/material.dart';

class BoardItem extends StatelessWidget {
  final String boardName;
  final void Function() onPressed;

  const BoardItem({Key key, this.boardName, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 266.6,
      height: 266.6,
      margin: const EdgeInsets.only(right: 20.4),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffD9D9D9)),
        borderRadius: BorderRadius.circular(4),
      ),
      child: Center(
        child: boardName == null
            ? Tooltip(
                message: 'NEW BOARD',
                decoration: BoxDecoration(
                  color: Color(0xff3C3C3C),
                  borderRadius: BorderRadius.circular(4),
                ),
                verticalOffset: 38,
                child: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: onPressed,
                ),
              )
            : Text(
                boardName.toUpperCase(),
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1,
                ),
              ),
      ),
    );
  }
}
