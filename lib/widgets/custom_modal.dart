import 'package:flutter/material.dart';

class CustomModal extends StatelessWidget {
  final void Function() onCreate;
  final TextEditingController controller;

  const CustomModal(
      {Key key, @required this.controller, @required this.onCreate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      titlePadding: const EdgeInsets.fromLTRB(32, 32, 32, 19),
      contentPadding: const EdgeInsets.fromLTRB(32, 0, 32, 32),
      insetPadding: const EdgeInsets.all(0),
      shape: const RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(20),
        ),
      ),
      title: Stack(
        children: [
          Center(
            child: Text(
              'NEW BOARD',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: const Color(0xff3C3C3C),
              ),
            ),
          ),
          Positioned(
            right: -10,
            top: -10,
            child: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
              },
              splashRadius: 20,
            ),
          )
        ],
      ),
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.25,
          child: Column(
            children: [
              TextField(
                controller: controller,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w100,
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: const Color(0xff3C3C3C),
                    ),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(28.0),
                    ),
                  ),
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 32.0,
                    vertical: 17.5,
                  ),
                ),
              ),
              SizedBox(height: 19),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: onCreate,
                    child: Text('CREATE'),
                    style: ElevatedButton.styleFrom(
                      elevation: 4,
                      shadowColor: const Color(0xff707070),
                      padding: const EdgeInsets.fromLTRB(38, 8, 38, 6),
                      primary: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18),
                      ),
                      textStyle: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
      titleTextStyle: TextStyle(fontSize: 16),
    );
  }
}
